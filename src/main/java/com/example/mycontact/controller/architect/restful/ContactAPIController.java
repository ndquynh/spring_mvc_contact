package com.example.mycontact.controller.architect.restful;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.example.mycontact.entity.Contact;
import com.example.mycontact.service.ContactService;

@RestController
public class ContactAPIController {

	@Autowired
	private ContactService contactService;
	
	@GetMapping("/contact/edit/{id}")
	public Contact edit(@PathVariable int id) {
//		model.addAttribute("contact", contactService.findOne(id));
		return contactService.findOne(id);
	}
}
