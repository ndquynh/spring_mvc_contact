package com.example.mycontact.exception;

public class StorageException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2070052594454504189L;

	public StorageException(String message) {
        super(message);
    }

    public StorageException(String message, Throwable cause) {
        super(message, cause);
    }
}
