package com.example.mycontact.service;

import java.nio.file.Path;
import java.util.stream.Stream;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

/**
 * https://spring.io/guides/gs/uploading-files/
 * @author nducquynh
 *
 */
public interface StorageService {

	void init();
	
	void store(MultipartFile multipartFile);
	
	Stream<Path> loadAll();
	
	Path load(String fileName);
	
	Resource loadAsResource(String fileName);
	
	void deleteAll();
	
}
