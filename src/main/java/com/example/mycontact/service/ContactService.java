/**
 * 
 */
package com.example.mycontact.service;

import java.util.List;

import com.example.mycontact.entity.Contact;

/**
 * @author ndquynh
 *
 */
public interface ContactService {

	Iterable<Contact> findAll();
	
	List<Contact> search(String q);
	
	Contact findOne(int id);
	
	void save(Contact contact);
	
	void delete(int id);
	
}
