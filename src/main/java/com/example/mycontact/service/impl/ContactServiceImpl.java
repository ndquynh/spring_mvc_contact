package com.example.mycontact.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.mycontact.entity.Contact;
import com.example.mycontact.repository.ContactRepository;
import com.example.mycontact.service.ContactService;

@Service
public class ContactServiceImpl implements ContactService {

	@Autowired
	private ContactRepository contactRepository;

	@Override
	public Iterable<Contact> findAll() {
		// TODO Auto-generated method stub
		return contactRepository.findAll();
	}

	@Override
	public List<Contact> search(String q) {
		// TODO Auto-generated method stub
		return contactRepository.findByNameContaining(q);
	}

	@Override
	public Contact findOne(int id) {

		Optional<Contact> optional = contactRepository.findById(id);
		
		if(optional.isPresent()) {
			return optional.get();
		} else {
			return null;
		}
	}

	@Override
	public void save(Contact contact) {
		// TODO Auto-generated method stub
		contactRepository.save(contact);
	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		contactRepository.deleteById(id);
	}
	
}
