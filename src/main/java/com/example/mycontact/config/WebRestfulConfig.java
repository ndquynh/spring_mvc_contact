package com.example.mycontact.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import com.example.mycontact.entity.Quote;

/**
 * example from https://spring.io/guides/gs/consuming-rest/
 * 
 */
@Configuration
public class WebRestfulConfig {
	private static final Logger log = LoggerFactory.getLogger(WebRestfulConfig.class);
	
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}
	
	/**
	 * example from https://spring.io/guides/gs/consuming-rest/
	 * @param url
	 */
	public Quote consumingRestAPI() {
		RestTemplate restTemplate = new RestTemplate();
		Quote quote = restTemplate.getForObject("http://gturnquist-quoters.cfapps.io/api/random", Quote.class);
		log.info(quote.toString() + "dasdasdasdasd");
		return quote;
	}
	
	@Bean
	public CommandLineRunner run(RestTemplate restTemplate) throws Exception {
		return args -> {
			Quote quote = restTemplate.getForObject("http://gturnquist-quoters.cfapps.io/api/random", Quote.class);
			log.info( " ___________ " + quote.toString());
		};
	}
}
