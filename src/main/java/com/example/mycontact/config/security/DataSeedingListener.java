package com.example.mycontact.config.security;

import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.example.mycontact.entity.security.Role;
import com.example.mycontact.entity.security.User;
import com.example.mycontact.repository.security.RoleRepository;
import com.example.mycontact.repository.security.UserRepository;

@Component
public class DataSeedingListener implements ApplicationListener<ContextRefreshedEvent> {

	final String ROLE_MEMBER = "ROLE_MEMBER";
	final String ROLE_ADMIN = "ROLE_ADMIN";
	
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private RoleRepository roleRepo;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		// Roles
		if(roleRepo.findByName(ROLE_ADMIN) == null) {
			roleRepo.save(new Role(ROLE_ADMIN));
		}
		
		if(roleRepo.findByName(ROLE_MEMBER) == null) {
			roleRepo.save(new Role(ROLE_MEMBER));
		}
		
		if(userRepo.findByEmail("admin@domain.com") == null) {
			User admin = new User();
			admin.setEmail("admin@domain.com");
			admin.setPassword(passwordEncoder.encode("123456"));
			HashSet<Role> roles = new HashSet<>();
			roles.add(roleRepo.findByName(ROLE_ADMIN));
			roles.add(roleRepo.findByName(ROLE_MEMBER));
			admin.setRoles(roles);
			userRepo.save(admin);
		}
		
		if(userRepo.findByEmail("member@domain.com") == null) {
			User admin = new User();
			admin.setEmail("member@domain.com");
			admin.setPassword(passwordEncoder.encode("123456"));
			HashSet<Role> roles = new HashSet<>();
			roles.add(roleRepo.findByName(ROLE_MEMBER));
			admin.setRoles(roles);
			userRepo.save(admin);
		}
	}

}
 