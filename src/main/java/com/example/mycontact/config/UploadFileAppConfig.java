package com.example.mycontact.config;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.mycontact.properties.StorageProperties;
import com.example.mycontact.service.StorageService;

/**
 * https://spring.io/guides/gs/uploading-files/
 * @author nducquynh
 *
 */
@Configuration
@EnableConfigurationProperties(StorageProperties.class)
public class UploadFileAppConfig {

	@Bean
    CommandLineRunner init(StorageService storageService) {
        return (args) -> {
//            storageService.deleteAll();
            storageService.init();
        };
    }
}
