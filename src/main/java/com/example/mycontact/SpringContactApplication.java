package com.example.mycontact;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@EnableAutoConfiguration(exclude = {SecurityAutoConfiguration.class})//this is for disabling the Spring security by default
public class SpringContactApplication {

	private static final Logger log = LoggerFactory.getLogger(SpringContactApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(SpringContactApplication.class, args);
	}

}
