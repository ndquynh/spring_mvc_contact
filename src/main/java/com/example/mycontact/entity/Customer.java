package com.example.mycontact.entity;

/**
 * https://spring.io/guides/gs/relational-data-access/
 * @author nducquynh
 *
 */
public class Customer {

	private long id;
	private String firstName, lastName;
	public Customer(long id, String firstName, String lastName) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	@Override
	public String toString() {
		return "Customer [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + "]";
	}
	
	
}
