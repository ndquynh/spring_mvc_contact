package com.example.mycontact.repository.security;

import org.springframework.data.repository.CrudRepository;

import com.example.mycontact.entity.security.User;

public interface UserRepository extends CrudRepository<User, Integer> {

	User findByEmail(String email);
}
