package com.example.mycontact.repository.security;

import org.springframework.data.repository.CrudRepository;

import com.example.mycontact.entity.security.Role;

public interface RoleRepository extends CrudRepository<Role, Integer> {

	Role findByName(String name);
}
