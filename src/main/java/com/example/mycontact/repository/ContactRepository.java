/**
 * 
 */
package com.example.mycontact.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.example.mycontact.entity.Contact;

/**
 * @author ndquynh
 *
 */
public interface ContactRepository extends CrudRepository<Contact, Integer> {

	List<Contact> findByNameContaining(String q);
}
